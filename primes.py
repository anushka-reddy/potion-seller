def largest_prime(k: int) -> int:
    """Method to compute the closest (smaller) prime number to the number entered by user,
    Worst case complexity is O(nlogn, where n is k"""
    for x in range(k-1,1,-1): #iterating backwards
        prime=True #setting prime flag to True
        for y in range(2,x//2):
            if x%y==0: #if the number x is divisible by a number other than itself or 1
                prime=False #need to set prime to False
        if prime==True: #if prime remains True, we return the x value
            return x

