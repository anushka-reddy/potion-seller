

from bst import BinarySearchTree
from typing import TypeVar, Generic
from node import AVLTreeNode
from node import TreeNode
from bst import BSTInOrderIterator


K = TypeVar('K')
I = TypeVar('I')


class AVLTree(BinarySearchTree, Generic[K, I]):
    """ Self-balancing binary search tree using rebalancing by sub-tree
        rotations of Adelson-Velsky and Landis (AVL).
    """

    def __init__(self) -> None:
        """
            Initialises an empty Binary Search Tree
            :complexity: O(1)
        """

        BinarySearchTree.__init__(self)

    def get_height(self, current: AVLTreeNode) -> int:
        """
            Get the height of a node. Return current.height if current is 
            not None. Otherwise, return 0.
            :complexity: O(1)
        """

        if current is not None:
            return current.height
        return 0

    def get_balance(self, current: AVLTreeNode) -> int:
        """
            Compute the balance factor for the current sub-tree as the value
            (right.height - left.height). If current is None, return 0.
            :complexity: O(1)
        """

        if current is None:
            return 0
        return self.get_height(current.right) - self.get_height(current.left)

    def insert_aux(self, current: AVLTreeNode, key: K, item: I) -> AVLTreeNode:
        """
            Attempts to insert an item into the tree, it uses the Key to insert
            it. After insertion, performs sub-tree rotation whenever it becomes
            unbalanced.
            returns the new root of the subtree.
            Complexity: If the tree is balanced then best case complexity would be O(compK),
            where compK is the complexity of comparing the keys.
            If the tree is unbalanced then the worst case complexity would be O(D)*(compK),
            where D is the depth of the tree.
        """
        #insert the current node using the insert_aux method in bst file, and storing the result in current
        current=BinarySearchTree.insert_aux(self,current,key,item)


        #next we need to set currents height, we compute this by checking the heights of its child nodes, taking the max of the two, then adding the max by 1 and storing this as the new height
        current.height = 1 + max(self.get_height(current.left),
                           self.get_height(current.right))

        #call rebalance to rebalance the tree
        return self.rebalance(current)



    def delete_aux(self, current: AVLTreeNode, key: K) -> AVLTreeNode:
        """
            Attempts to delete an item from the tree, it uses the Key to
            determine the node to delete. After deletion,
            performs sub-tree rotation whenever it becomes unbalanced.
            returns the new root of the subtree.
            Complexity: If the tree is balanced then best case complexity would be O(n),
            where n is the number of nodes in the tree, this is calculated by taking into
            account the complexity of the method for calculating the heights.
            If the tree is unbalanced then the worst case complexity would be O(n)*(compK),
            where n is the number of nodes in the tree and compK is the complexity of comparing the keys.
        """

        #first we need to call the delete method in binary tree, to delete the node based off the key value provided
        #current contains the new root of the subtree
        current=BinarySearchTree.delete_aux(self,current,key)

        #return None if current is none
        if current is None:
            return current

        #update the heights of currents' child nodes using the calculate height method
        if current.left is not None:
            current.left.height=self.calculate_height(current.left)
        if current.right is not None:
            current.right.height=self.calculate_height(current.right)
            

        #update currents height based off its childs heights
        current.height = 1 + max(self.get_height(current.left),
                   self.get_height(current.right))
        
        #call rebalance to rebalance the tree
        return self.rebalance(current)



    def left_rotate(self, current: AVLTreeNode) -> AVLTreeNode:
        """
            Perform left rotation of the sub-tree.
            Right child of the current node, i.e. of the root of the target
            sub-tree, should become the new root of the sub-tree.
            returns the new root of the subtree.
            Example:

                 current                                       child
                /       \                                      /   \
            l-tree     child           -------->        current     r-tree
                      /     \                           /     \
                 center     r-tree                 l-tree     center

            :complexity: O(1)
        """
        # to switch the nodes

        # new root should be at the right of current initially
        new_root = current.right
        #storing the new right of current which would be the new_roots node to its left
        new_current_right = new_root.left

        #new roots left should be switched to current
        new_root.left = current
        #currents right should switch the the new currents initial right
        current.right = new_current_right
        #a right rotate will increase 1 for right size of subtree
        current.right_size = new_current_right.right_size + 1

        #to update the heights
        #to update currents height and new_roots height, we need to consider its child nodes' heights, we should take the max of the two heights and then add it by 1
        current.height = 1 + max(self.get_height(current.left),
                         self.get_height(current.right))
        new_root.height = 1 + max(self.get_height(new_root.left),
                         self.get_height(new_root.right))
        
        return new_root


    def right_rotate(self, current: AVLTreeNode) -> AVLTreeNode:
        """
            Perform right rotation of the sub-tree.
            Left child of the current node, i.e. of the root of the target
            sub-tree, should become the new root of the sub-tree.
            returns the new root of the subtree.
            Example:

                       current                                child
                      /       \                              /     \
                  child       r-tree     --------->     l-tree     current
                 /     \                                           /     \
            l-tree     center                                 center     r-tree

            :complexity: O(1)
        """
        # new root should be at the left of current initially
        new_root = current.left
        #storing the new left of current, which would be the new_roots node to its right
        new_current_left = new_root.right

        #new roots right should be switched to current
        new_root.right = current
        #a right rotate will increase 1 for right size of subtree
        new_root.right_size = current.right_size + 1
        #currents left should switch the the new currents initial left
        current.left = new_current_left
        
        #to update currents height and new_roots height, we need to consider its child nodes' heights, we should take the max of the two heights and then add it by 1
        current.height = 1 + max(self.get_height(current.left),
                        self.get_height(current.right))
        new_root.height = 1 + max(self.get_height(new_root.left),
                        self.get_height(new_root.right))
 
        return new_root

    def rebalance(self, current: AVLTreeNode) -> AVLTreeNode:
        """ Compute the balance of the current node.
            Do rebalancing of the sub-tree of this node if necessary.
            Rebalancing should be done either by:
            - one left rotate
            - one right rotate
            - a combination of left + right rotate
            - a combination of right + left rotate
            returns the new root of the subtree.
        """
        if self.get_balance(current) >= 2:
            child = current.right
            if self.get_height(child.left) > self.get_height(child.right):
                current.right = self.right_rotate(child)
            return self.left_rotate(current)

        if self.get_balance(current) <= -2:
            child = current.left
            if self.get_height(child.right) > self.get_height(child.left):
                current.left = self.left_rotate(child)
            return self.right_rotate(current)
        self.length += 1
        return current

    def kth_largest(self, k: int) -> AVLTreeNode:
        """
        Returns the kth largest element in the tree.
        k=1 would return the largest.
        best cases: O(1)
        worst cases: O(logn), where n is the number of nodes in the tree
        
        """
        if k <= 0:
            # When  given k is not valid positive values
            raise KeyError("Invalid node position ")
        elif self.root is None:
            # When Binary Search Tree have no elements
            raise ValueError("Empty Binary Search Tree")
        else:
            current = self.get_root() 
            return self.kth_largest_aux(current, k)

   
    def kth_largest_aux(self,current:AVLTreeNode,k:int)->int:
        """
        have a new attribute for nodes, this will show how many node in its right
        subtree. Then just compare with kth value. And if k is in the left
        subtree, in the recursion we need to have k value minus the current right
        subree nodes plus one in order to have a right kth largest.
        best cases: O(1)
        worst cases: O(logn), where n is the number of nodes in the tree.
        """
        
        if current is None: #if the root is None, return none
            return None
        
        if current.right_size + 1 == k: # right size +1 mean how many nodes in the right and included itself.
            return current # when we find the kth largest node, return it.
        
        if current.right_size >=k: # when k is smaller than right size means k is in the right subtree.
            return self.kth_largest_aux(current.right, k)
        else: # current.right_size < k, means its on the left subtree
            return self.kth_largest_aux(current.left, k-(current.right_size+1))
            
        

    def calculate_height(self,current:AVLTreeNode)->int:
        """Method to use recursion to compute the height of a given node
        Complexity: Best and worst case is O(n), where n is the number of nodes in the tree"""
        
        #base case is when we reach the empty node, the height is -1
        if current is None:
            return -1
         
        # recurisvely call the method to compute the max height of either the left or right, then add one to the final value and return this as the height of the node
        return 1 + max(self.calculate_height(current.left), self.calculate_height(current.right))

        
        
        
        
