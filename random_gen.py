from typing import Generator

def lcg(modulus: int, a: int, c: int, seed: int) -> Generator[int, None, None]:
    """Linear congruential generator."""

    while True:
        seed = (a * seed + c) % modulus
        yield seed


class RandomGen:
    
    def __init__(self, seed:int=0) -> None:
        """Init methods sets the values as provided, and calls the function
        lcg return with these values. Complexity: O(1) for best and worst.
        """
        modulus_val=pow(2,32)
        a_val=134775813
        c_val=1
        seed_val=seed
        self.lcg_return=lcg(modulus_val, a_val, c_val, seed_val)



    def randint(self, k: int) -> int:
        """This method takes the first 5 numbers from the generator and stores them. Then it
        converts these numbers to binary strings. Then it calls the method which will generate
        a new number, which would contains a 1 in each bit if at least 3 of 5 generated
        random numbers have a 1 in this position. This method will also convert the new number
        into its decimal form, and will then return the new number % k + 1
        Complexity: O(n), for best and worst where n is the size of the list.
        """
        # creating a list of size 5 which will contain the random numbers
        random_numbers=[next(self.lcg_return) for _ in range(5)]
        #convert these numbers into binary and remove the 16 lsb
        for x in range(len(random_numbers)):
            random_numbers[x]=self.decimal_to_binary(random_numbers[x])
        #check how many ones in each of the number
        new_number=self.generate_new_num(random_numbers)
        #return number % k + 1
        return new_number % k +1


    def decimal_to_binary(self, number:int) -> str:
        """This method first converts the number into a binary number and ensures the binary string
        it will output, will contain 16 bits. Big O Complexity is O(1),for both best and worst."""
        #number is converted to its binary form, also need to remove '0b', since this is not needed
        binary_string=(bin(number)[2:])

        #if the number of bits is less than 16, we need to make the string contain 16 bits
        if len(binary_string)<16:
            char=16-len(binary_string)
            new_str='0'*char
            new_str+=binary_string
            binary_string=new_str
        else:
            #if not, we need to remove the 16 lsb
            binary_string=binary_string[:-16]
            #then we need to check if the number of bits is less than 16, if true we need to change the string so that it contains 16 bits
            if len(binary_string)<16:
                char=16-len(binary_string)
                new_str=char*'0'
                new_str+=binary_string
                binary_string=new_str
        #return the binary string
        return binary_string

    def binary_to_decimal(self, binary:bin) -> int:
        """Method to convert a binary number into a decimal number
        Complexity: O(1), for best and worst """
        return int(binary,2)

    def generate_new_num(self, random_numbers:list) ->int:
        """Generate a new number, which is 16 bits long and has a 1 in each bit if at least 3 of
        the 5 generated numbers have a 1 in this bit. Complexity: O(n), for best and worst, where n is the size of the
        list of random numbers."""
        new_num=''
        #iterate through each digit
        for digit in range(16):
            count=0
            #iterate through the random numbers
            for num in range(len(random_numbers)):

                #pos contains the value of the bit at the current number and current digit
                pos=random_numbers[num][digit]
                #if the bit is 1, increment count
                if pos=='1':
                    count+=1
            #if count is more than or equal to three, the new number should contain a 1 at this position
            if count>=3:
                new_num+='1'
            else:
                #if not should contain a zero
                new_num+='0'
        return self.binary_to_decimal((new_num))
