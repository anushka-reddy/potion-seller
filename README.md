## README: Potion Seller

### Introduction
This document outlines the game and implementation of a bartering game called "Potion Seller." The game involves vendors, adventurers, and players engaging in transactions with potions to maximize profits.

### Problem Background - Potion Seller
The Potion Seller game involves vendors selling potions to adventurers through the player, aiming to maximize profits. Potions have attributes such as category, name, and price per litre. Vendors select potions from their inventory each day, and players buy and sell potions to adventurers.

### Player Rules
The potion seller game is a simple optimization problem where you, as the player, aim to maximize your profit by buying and selling potions from a vendor. Here's an overview of how the game works:
Vendor Information: You are provided with information about the potions available for sale and their prices per litre. Each potion has a different price.
Starting Money: You start the game with a certain amount of money.
Objective: Your objective is to maximize your profit by buying and selling potions.
Gameplay:
You can purchase potions from the vendor using your starting money.
Each potion you buy has a cost associated with it, and you must decide which potions to buy to maximize your profit.
After purchasing potions, you can sell them to adventurers. Each potion type has a different selling price per litre.
The goal is to buy potions at the lowest possible cost and sell them at the highest possible price to maximize your profit.
Optimization: The game can be approached as an optimization problem, where you need to find the combination of potions to buy that maximizes your profit given your starting money.

## Initializing

```python
G = Game()
# There are these potions, with these stats, available over the course of the game.
G.set_total_potion_data([
# Name, Category, Buying price from vendors.
[“Potion of Health Regeneration”, “Health”, 20],
[“Potion of Extreme Speed”, “Buff”, 10],
[“Potion of Deadly Poison”, “Damage”, 45],
[“Potion of Instant Health”, “Health”, 5],
[“Potion of Increased Stamina”, “Buff”, 25],
[“Potion of Untenable Odour”, “Damage”, 1],
])
# Start of Day 1
# Let’s begin by adding to the inventory of PotionCorp:
G.add_potions_to_inventory([
(“Potion of Health Regeneration”, 4),
(“Potion of Extreme Speed”, 5),
(“Potion of Instant Health”, 3),
(“Potion of Increased Stamina”, 10),
(“Potion of Untenable Odour”, 5),
])
# Now for choosing vendors!
selling = G.choose_potions_for_vendors(4)
print(selling)
>>> [
(“Potion of Extreme Speed”, 5),
(“Potion of Increased Stamina”, 10),
(“Potion of Health Regeneration”, 4),
(“Potion of Instant Health”, 3),
]
```
### Explanation
● In this example, vendor 1 picked a random number from 1 to 5 and got 3, since
Extreme Speed is the 3rd most expensive in inventory.
● vendor 2 picked a random number from 1 to 4 and got 1, since Increased Stamina is
the 1st most expensive in the remaining inventory.
● vendor 3 picked a random number from 1 to 3 and got 1, since Health Regeneration
is the 1st most expensive in the remaining inventory.
● vendor 4 picked a random number from 1 to 2 and got 1, since Instant Health is the
1st most expensive in the remaining inventory

## Example Usage in Game Mode

```python
# Import the game module
import game_module as G

# Define the full vendor information
full_vendor_info = [
    ("Potion of Health Regeneration", 30),
    ("Potion of Extreme Speed", 15),
    ("Potion of Instant Health", 15),
    ("Potion of Increased Stamina", 20),
]

# Play the game with 3 attempts, at different starting money.
results = G.solve_game_1(full_vendor_info, [12.5, 45, 80])
print(results)
# Output: [37.5, 90, 142.5]
```
### Why this is this the output?

This result is achieved for the following reasons:
Attempt 1 can make $37.5 by buying 2.5 litres of Instant Health for $12.5.
Attempt 2 can make $90 by buying 3 litres of Instant Health for $15, and 1 litre of Health
Regeneration for $20, and 1 litre of Extreme Speed for $10.
Attempt 3 can make $142.5 by buying 3 litres of Instant Health for $15, 3 litres of Health
Regeneration for $60, and 0.5 litres of Extreme Speed for $5.

### Functions Descriptions
1. **Prime Number Generation**: Implemented a function to generate the largest prime number less than a given integer using the Sieve of Eratosthenes.
2. **Hashing Potions**: Defined a Potion class with attributes and methods for potion management, including hashing functions.
3. **Hash Table Analysis**: Implemented a hash table with functionality to analyze conflicts, probes, and collision handling.
4. **BST Operations**: Extended a Binary Search Tree (BST) class with methods for finding successors and minimal nodes.
5. **Self-balancing AVL Trees**: Implemented AVL trees with methods for left and right rotations to maintain balance.
6. **Playing the Game**: Developed methods for managing potion inventory, selecting potions for vendors, and simulating the game process.
7. **Game Mode**: Implemented a method to solve the game optimally, maximizing profits with different starting balances.

### Documentation Requirements
- All code files include Python documentation with explanations of approaches and worst-case time complexities.
- Additional documentation for methods with complexity requirements, including explanations of algorithm complexities.
- Detailed explanations and inline comments for the `solve_game` method to clarify the solution approach.

### Conclusion
The solution aim to efficiently implement the functionalities required for the Potion Seller game, including data structures like hash tables and AVL trees, as well as algorithms for game management. Clear documentation and explanations are provided to ensure the understanding and effectiveness of the solutions.
