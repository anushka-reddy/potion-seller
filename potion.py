class Potion:
    """Class which sets Potion attributes and computes hash values for these potions"""
    
    def __init__(self, potion_type: str, name: str, buy_price: float, quantity: float) -> None:
        """Initialization of instance variables.
        Complexity: Best=Worst, O(1)"""
        self.potion_type=potion_type
        self.name=name
        self.buy_price=buy_price
        self.quantity=quantity

    @classmethod
    def create_empty(cls, potion_type: str, name: str, buy_price: float) -> 'Potion':
        """Creating a potion with a quantity of 0.
        Complexity: Best=worst, O(1)"""
        x=cls(potion_type, name, buy_price, 0) #creating a instance of a potion, x, with a quantity of 0
        return x
        

    @classmethod
    def good_hash(cls, potion_name: str, tablesize: int) -> int:
        """Creates a hash value based off all letters in the string, potion name. This method takes each character and then computes its ascii value and adds it to the noise multiplied by the value, modulo the tablesize. Then it updates the noise by             noise = (noise * hash_base) % (tablesize - 1)
        multiplying the noise by the value stored in hash_base modulo the tablesize -1.
        Complexity: Best case= Worst Case, which is O(n), where n is the number of characters in the potion name.
        """
        value = 0
        noise = 7643 #should be a prime
        hash_base=31 #also should be prime

        for char in potion_name:
            value = (ord(char)+noise * value) % tablesize
            noise = (noise * hash_base) % (tablesize - 1)
        return value 

    @classmethod
    def bad_hash(cls, potion_name: str, tablesize: int) -> int:
        """This method only takes into account the first letter of the potion name, therefore potion names which start with the same letter, will both hash to the same value
        Complexity: Best=Worst, which is O(1)"""
        return ord(potion_name[0]) % tablesize



 
