""" Hash Table ADT

Defines a Hash Table using Linear Probing for conflict resolution.
It currently rehashes the primary cluster to handle deletion.
"""
__author__ = 'Brendon Taylor, modified by Jackson Goerner'
__docformat__ = 'reStructuredText'
__modified__ = '21/05/2020'
__since__ = '14/05/2020'

from referential_array import ArrayR
from typing import TypeVar, Generic
from potion import Potion
T = TypeVar('T')


class LinearProbePotionTable(Generic[T]):
    """
    Linear Probe Potion Table

    This potion table does not support deletion.

    attributes:
        count: number of elements in the hash table
        table: used to represent our internal array
        table_size: current size of the hash table
    """

    def __init__(self, max_potions: int, good_hash: bool=True, tablesize_override: int=-1) -> None:
        """This method initializes variables to their starting values. This method also checks if the tablesize_override is -1, and if True it calls a method
        which will create an Array and will initialize the table size to the number of max_potions. On the other hand, if the tablesize_override is not -1, this method
        will call the same which will create an array with the value of tablesize_override as the table_size.
        The best and worst case of this method is O(1)"""
        # Statistic setting
        #setting instance variables
        self.conflict_count = 0
        self.probe_max = 0
        self.probe_total = 0
        self.max_potions=max_potions
        #check the hash setting, and set the mode accordingly 
        if good_hash==True:
            self.hash_mode='good_mode'
        else:
            self.hash_mode='bad_mode'
        #check tablesize_override and call the method to create the array with the appropriate table_size
        if tablesize_override==-1:
            self.initalise_with_tablesize(max_potions)
        else:
            self.initalise_with_tablesize(tablesize_override)


    def hash(self, potion_name: str) -> int:
        """This method checks what the user set the hash mode to. If the hash mode is set to 'good_mode', then the method will call the good_hash method from the Potion file.
        If the hash mode is otherwise, then the method will call the bad_hash method from the Potion file.
        The worst-case complexity of this method is O(n) when the hash mode is set to good_mode, where n is the number of characters in the potion name.
        The best-case comlexity of this method is O(1), when hash mode is set to bad_mode. """
        if self.hash_mode=='good_mode':
            hash_val=Potion.good_hash(potion_name,self.table_size)
        else:
            hash_val=Potion.bad_hash(potion_name,self.table_size)
        return hash_val


    def statistics(self) -> tuple:
        """This method will return a tuple, which will contain the total number of conflicts, the total number of probes and the size of the largest probe chain.
        Complexity is O(1), for best and worst case."""
        return (self.conflict_count, self.probe_total, self.probe_max)

    def __len__(self) -> int:
        """
        Returns number of elements in the hash table
        :complexity: O(1)
        """
        return self.count

    def __linear_probe(self, key: str, is_insert: bool) -> int:
        """
        Find the correct position for this key in the hash table using linear probing
        :complexity best: O(K) first position is empty
                          where K is the size of the key
        :complexity worst: O(K + N) when we've searched the entire table
                           where N is the table_size
        :raises KeyError: When a position can't be found
        """
        position = self.hash(key)  # to get a postion for the key using the hash set by the user
        base_position = position #set the starting position as a base, so we have a reference
        initial_probe_max=0 #set the intiial probe max to 0
        if is_insert and self.is_full():
            raise KeyError(key)

        for _ in range(len(self.table)):  # iterate through the table
            if self.table[position] is None:  # found empty slot
                if is_insert:
                    return position
                else:
                    raise KeyError(key)  # so the key is not in
            elif self.table[position][0] == key:  # found key
                return position
            else:  # there is something but not the key, try next, we need to begin probing 
                self.probe_total+=1 #probe total will increase by 1, since we need to find the next empty spot
                initial_probe_max+=1 #probe max will increase by 1 so we can keep track of how many times we have to probe for the given key 
                if base_position==position:#if postion is occupied, and it is the same position our hash returned, and the keys are not identical, we need to increment the conflict count
                    self.conflict_count+=1
                position = (position + 1) % len(self.table)
            if self.probe_max<initial_probe_max: #lastly, we need to check if the current probe max is more than the initial probe max, if true we need to set the probe max, to the inital value
                self.probe_max=initial_probe_max

        raise KeyError(key)

    def __contains__(self, key: str) -> bool:
        """
        Checks to see if the given key is in the Hash Table
        :see: #self.__getitem__(self, key: str)
        """
        try:
            _ = self[key]
        except KeyError:
            return False
        else:
            return True

    def __getitem__(self, key: str) -> T:
        """
        Get the item at a certain key
        :see: #self.__linear_probe(key: str, is_insert: bool)
        :raises KeyError: when the item doesn't exist
        """
        position = self.__linear_probe(key, False)
        return self.table[position][1]

    def __setitem__(self, key: str, data: T) -> None:
        """
        Set an (key, data) pair in our hash table
        :see: #self.__linear_probe(key: str, is_insert: bool)
        :see: #self.__contains__(key: str)
        """
        if len(self) == len(self.table) and key not in self:
            raise ValueError("Cannot insert into a full table.")
        position = self.__linear_probe(key, True)

        if self.table[position] is None:
            self.count += 1
        self.table[position] = (key, data)

    def initalise_with_tablesize(self, tablesize: int) -> None:
        """
        Initialise a new array, with table size given by tablesize.
        Complexity: O(n), where n is len(tablesize)
        """
        self.count = 0
        self.table = ArrayR(tablesize)

    def is_empty(self):
        """
        Returns whether the hash table is empty
        :complexity: O(1)
        """
        return self.count == 0

    def is_full(self):
        """
        Returns whether the hash table is full
        :complexity: O(1)
        """
        return self.count == len(self.table)

    def insert(self, key: str, data: T) -> None:
        """
        Utility method to call our setitem method
        :see: #__setitem__(self, key: str, data: T)
        """
        self[key] = data

    def __str__(self) -> str:
        """
        Returns all they key/value pairs in our hash table (no particular order)
        :complexity: O(N) where N is the table size
        """
        result = ""
        for item in self.table:
            if item is not None:
                (key, value) = item
                result += "(" + str(key) + "," + str(value) + ")\n"
        return result
