from __future__ import annotations
# ^ In case you aren't on Python 3.10

from random_gen import RandomGen
from potion import Potion
import copy
from referential_array import ArrayR
from abstract_list import List, T
from avl import AVLTree

class Game:
    """In our implementation of solve game, we used a list as we would have constant access time to any element in the list.
    By using a list, we were also able to use binary search which helped keep a lower complexity on some methods. """
    
    def __init__(self, seed=0) -> None:
        """"""
        self.rand = RandomGen(seed=seed)
    
    def set_total_potion_data(self, potion_data: list) -> None:
        """Sets the values to inital values. Iterates through the potion data provided and assigns the
        name, type, price accordingly. Then it calls the create empty located in potion, to set the quantity
        of these potions to 0. Finally, this method calls bubble sort to sort the potion inventory in an increasing order
        by taking into account the name for each potion.
        Complexity for best and worst is O(n**2) where n is the size of the potion_inventory, since we are calling bubble sort."""
        #initialize variables 
        self.potion_data=potion_data
        self.potion_inventory= []
        self.initial_inventory=[]
        #iterate through potion_data
        for x in range(len(potion_data)):
            name=potion_data[x][0]
            potion_type=potion_data[x][1]
            buy_price=potion_data[x][2]
            #create a potion with a quantity of 0
            empty_potion=Potion.create_empty(potion_type,name,buy_price)
            #store it into inventory
            self.potion_inventory.append(empty_potion)
        #sort inevntory by name in a non-decreasing order
        self.bubbleSort()
        
        
    def bubbleSort(self):
        """Sort potions inventory by name in a non-decreasing order.
        Complexity: O(n**2) for best and worst case, since there is no earlu exit condition."""
        for x in range(len(self.potion_inventory)):
            for j in range(0, len(self.potion_inventory)-x-1):
                #check if the name in the jth position is larger than the name in the j+1th place, if true then swap
                if  self.potion_inventory[j].name >  self.potion_inventory[j+1].name:
                    self.potion_inventory[j], self.potion_inventory[j+1] = self.potion_inventory[j+1], self.potion_inventory[j]
        

    def add_potions_to_inventory(self, potion_name_amount_pairs: list[tuple[str, float]]) -> None:
        """This is a method which adds potions' quantities to the already existing potions, in the potion inventory.
        The best case complexity is O(CxLogN), where c is the length of potion_name_amount_pairs, and n is the number of
        potions in the inventory. This is beacuse we are iterating through the potion_name_amount_pairs and then calling
         binary search each time which has complexity of logn."""
        #iterates through the potion amount pairs list
        for y in range(len(potion_name_amount_pairs)):
            #selects the yth potion name and assigns it as the potion to find
            potion_to_find=potion_name_amount_pairs[y][0]
            #assigns the index returned by binary search
            ind=self.binary_search(self.potion_inventory, 0, len(self.potion_inventory)-1, potion_to_find)
            if ind!=-1:
                #assigns the new quantity of the potion to the potion in the inventory with index ind
                self.potion_inventory[ind].quantity=potion_name_amount_pairs[y][1]
        #keep a copy of the inventory with the quanitites 
        self.initial_inventory=copy.deepcopy(self.potion_inventory)


    def choose_potions_for_vendors(self, num_vendors: int) -> list:
        """Method which takes into account the number of vendors, and assigns a random number to each of these vendors, using
        the random number generator created earlier..
        Then, using the random number it generates, calling this number p, the vendor then selects the pth most expensive
        potion.
        Complexity: O(nlogn) where n is the size of the inventory, this is because the largest factor affecting the complexity
        is calling merge sort to sort the potion_inventory."""
        #copying the list back
        self.potion_inventory=copy.deepcopy(self.initial_inventory)
        #sorting the list in decreasing order by using the buy_price as the key to sort it by
        self.merge_sort(self.potion_inventory)
        #reverse list because we want most expensive potions at the front
        self.potion_inventory = self.potion_inventory[::-1]
        count = 0
        self.selling=[]
        #creating an instance x of RandomGen
        x=RandomGen()
        #looping until we have assigned a potion to each vendor 
        while count!= num_vendors:
            random_num=x.randint(len(self.potion_inventory))  #assign a random number
            res=(self.potion_inventory[random_num-1].name,self.potion_inventory[random_num-1].quantity)# potion name and quantity
            del self.potion_inventory[random_num-1] #removing this potion from the inventory
            self.selling.append(res) #apending the potion name and quantity to the sellinglist
            count+=1
        return self.selling



    def binary_search(self, lst:list, low:int, high:int, x:int)-> int:
        """Binary search algorithm, which takes a list sorted in an increasing order, and finds the potion's index
        by searching for its name (x).
        Complexity is O(logn), where n is the size of the list."""
 
        if high >= low:
            mid = (high + low) // 2
 
            #if we found the element, we need to return the index
            if lst[mid].name == x:
                return mid
 
            #element is smaller than mid, then it has to be present in right subarray
            elif lst[mid].name > x:
                return self.binary_search(lst, low, mid - 1, x)
 
            #element is larger than mid, then it has to be present in left subarray
            else:
                return self.binary_search(lst, mid + 1, high, x)

        #element could not be found
        else:
            return -1


    def merge_sort(self, array: ArrayR, key=None):
        """
        This code is from lecture slides. merge the array in increasing order.
        Time complexity: O(n log(n))*CompEq, where n is the number of elements in the array and CompEq is the
        complexity of the comparison.
        """
        tmp = ArrayR(len(array))
        start = 0
        end = len(array) - 1
        self.merge_sort_aux(array, start, end, tmp, key)
        if key != 99:
            return array

    def merge_sort_aux(self, array: ArrayR, start: int, end: int, tmp: T, key) -> None:
        """
        This code is from lecture slides. split the array.
        """
        if not start == end:  # 2 or more still to sort
            mid = (start + end) // 2
            # split into two halves
            self.merge_sort_aux(array, start, mid, tmp, key)
            self.merge_sort_aux(array, mid + 1, end, tmp, key)
            # merge
            self.merge_arrays(array, start, mid, end, tmp, key)
            # copy tmp back into the original
            for i in range(start, end + 1):
                array[i] = tmp[i]

    def merge_arrays(self, a, start, mid, end, tmp, key) -> None:
        """
        This code is from lecture slides. merge the sub array
        """
        ia = start
        ib = mid + 1
        for k in range(start, end + 1):
            if ia > mid:  # a finished, copy b
                tmp[k] = a[ib]
                ib += 1
            elif ib > end:  # b finished, copy a
                tmp[k] = a[ia]
                ia += 1
            elif key!=None and a[ia][key] <= a[ib][key] :  # a[ia] is the item to copy
                tmp[k] = a[ia]
                ia += 1
            elif key == None and self.potion_inventory[ia].buy_price <= self.potion_inventory[ib].buy_price:  # a[ia] is the item to copy
                tmp[k] = a[ia]
                ia += 1
            else:
                tmp[k] = a[ib]  # b[ib] is the item to copy
                ib += 1
    
    def solve_game(self, potion_valuations: list[tuple[str, float]], starting_money: list[int]):
        """
        To calculate the maximum profit, this method finds the most profitable potions, then sorts all potions in decreasing order by
        the profit ratio, then players can buys as many high-profit potions as possible with their starting money. After the most profitable potion has a
        stock of 0, the method finds the next profitable potion and does the same. If the player can't afford a full litre of a potion,
        the method will find the next most profitable potion still in stock and use their remaining money to buy as much of that as possible.
        Time complexity: O(nlogn) +nm, Where N is the length of potion_valuations, and M is the length of starting_money
        """
        selling_info = []
        for name,sell_price in potion_valuations: # have a list with positive profit ration and non-empty potion
            ind = self.binary_search(self.potion_inventory,0,len(self.potion_inventory),name)  # search adventurers' potion in inventory and return the position
            if ind != -1: # if found
                potion = self.potion_inventory[ind] # potion = the potion we found
                if potion.quantity != 0 and sell_price > potion.buy_price: # if the potion is not empty and the profit is positive
                    ratio = (sell_price - potion.buy_price) / potion.buy_price  # profit ratio
                    selling_info.append((potion.name,ratio,potion.buy_price,potion.quantity)) # append a tuple into the list, which stores the information of the potion, including the profit ratio
        increasing_order = self.merge_sort(selling_info, 1) #sort selling info list based off the profit ratio in a increasing order
        selling_info = increasing_order[::-1] # reversing the list, so that the selling info is in a decreasing order, where the potion with the highest profit ratio is in the front
        
        profit_list = [] #stores the profit throughout the day, for each day
        for m in starting_money: #m is the current day's starting money
            candidates = list(selling_info) #deep copy selling info to candidates
            left_pointer = 0
            right_pointer = 1
            total_profit = m #current days current profit

            #it finds if any, potions with the same profit ratio, and then adds these potions to the list 'sub candidates', if its finds none with the same ratio, then it adds the potion with the highest profit ratio to sub candidates
            while m > 0 and left_pointer < len(candidates):  # when the starting money is not empty and while left pointer is less than the size of candidates
                while right_pointer < len(candidates): # find the potions with same profit ratio
                    if candidates[right_pointer][1] != candidates[left_pointer][1]: #if the potions' profit ratio is not equal
                        break  # if they're is not, just break
                    right_pointer += 1  # if they're equal, right pointer plus 1
                sub_candidates = candidates[left_pointer:right_pointer]  # the potion with same profit ratio if any, if there are no potions with the same profit ratio, it ill add the potion with the highest profit ratio
                left_pointer = right_pointer  # update pointers
                right_pointer += 1  # update pointers

                incr_order = self.merge_sort(sub_candidates, 2) # sorting the list sub-candidates, with the key as the buy_price of the potions
                sub_candidates = incr_order[::-1]  # reversing the list, so that the potion with the highest buy price would be at the front of the list

                # calculate the total profit.
                for name,ratio,buy_price,available_quantity in sub_candidates:  #iterate through sub-candidates
                    unit_price = buy_price / 10  # /10 to have 0.1 as unit
                    unit_profit = (ratio * buy_price)/10
                    bought = min((m - m%unit_price)/unit_price , available_quantity*10) #caluclating how many potions we can buy with the money we have and how many there are in the inventory
                    m -= bought * unit_price #updating our starting money
                    total_profit += unit_profit * bought #updating our profit
            
            profit_list.append(total_profit)  # append different days profit into list.
            
        return profit_list


